const express = require('express')
const app = express()
const { PORT = 8080 } = process.env
const path = require('path')


//Add the following lines to enable Body parsing in Express 
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Logic goes here

//Allow the express server to share static content (CSS, JS)
app.use('/assets', express.static( path.join(__dirname, 'public', 'static' ) ))

//Default route for website
app.get('/', (req, res)=> {
    return res.sendFile( path.join(__dirname, 'public', 'index.html') )
})

app.get('/restaurant', (req, res)=> {

    var fs = require('fs');
    var data;
    fs.readFile('data.json', 'utf8', function (err, data) {
      if (err) throw err;
      data = JSON.parse(data);
      
    });
      console.log(data)
      console.log("sup")
    return res.sendFile( path.join(__dirname, 'public', 'restaurant.html') )
})



app.get('/all', (req, res)=> {

    return res.sendFile( path.join(__dirname, 'data.json') )
})

app.get('/all/:id', (req, res)=> {
    //Reading id from URL
    const id = req.params.id
    console.log(id)
    // Searching restaurants for the id  
      

    return res.sendFile( path.join(__dirname, 'data.json') )
})

app.get('/pic', (req, res)=> {

    return res.sendFile( path.join(__dirname, 'public/static/css/holidayinn.jpeg') )
})

app.use('/images', express.static(__dirname + '/public/static/css/holidayinn.jpeg'));



app.delete("/users/:id", (req, res) => {
    readFile((data) => {
      // add the new user
      const userId = req.params["id"];
      delete data[userId];
  
      writeFile(JSON.stringify(data, null, 2), () => {
        res.status(200).send(`users id:${userId} removed`);
      });
    }, true);
  })

  

//Create and listen to port
app.listen(PORT, ()=> console.log(`Server started to port:! ${PORT}...`))

