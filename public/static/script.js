//Executing directly on load
window.onload = function() {
renderUsers()
};

//In "/all" the restaurants are printed as json, fetching them to "let url". 
async function getUsers() {

    let url = "/all"
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}

async function renderUsers() {
    let users = await getUsers();

    let html = '';
    users.map(user => {

        let htmlSegment = `<div class="user">
                    
                            <h1 class="title">
                            ${user.name} </h1>
                            <h2>
                            ${user.cuisine}</h2>
                            <h2>
                            ${user.location}</h2>
                            <h2>
                            ${user.rating}</h2>
                            <h2>
                            ${user.catchfrase}
                            </h2>
                           
                            <img src="/images"> 
                            </img>
                            
                            
                        </div>`;

        html += htmlSegment;
    });

    console.log(html)
    let container = document.querySelector('.container');
    container.innerHTML = html;
}

